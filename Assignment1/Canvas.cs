﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
                 
namespace Assignment1
{
    /// <summary>
    /// This canvas class is mainly to display all graphics funtionality in the output window
    /// Declared graphics,pen and hatchbrush
    /// </summary>
    class Canvas
    {
        Graphics g;
        Pen Pen;
        int xPos, yPos;
        HatchBrush Brush;
        /// <summary>
        /// In this public class , assigned x and y postion to the output window
        /// </summary>
        /// <param name="g">graphics as an argument</param>
        public Canvas (Graphics g)
        {
            this.g = g;
            xPos = yPos = 0;
            Pen = new Pen(Color.Red, 1);
            //Pen.Width = 1.0F;
            Brush = new HatchBrush(HatchStyle.Cross,Color.White,Color.Red);

        }
        /// <summary>
        /// Method to draw line
        /// </summary>
        /// <param name="toX">From point to draw a line</param>
        /// <param name="toY">To Point to draw a line</param>
        public void DrawLine(int toX,int toY)
        {
            g.DrawLine(Pen, xPos, yPos, toX, toY);
            xPos = toX;
            yPos = toY;
        }
        /// <summary>
        /// Change the cursor position
        /// </summary>
        /// <param name="X">From postion</param>
        /// <param name="Y">To position</param>

        public void MoveTo(int X, int Y)
        {
            xPos = X;
            yPos = Y;
           
        }
        /// <summary>
        /// Method to draw a circle
        /// </summary>
        /// <param name="radius">radius of the circle</param>
        public void DrawCircle (int radius)
        {
            g.DrawEllipse(Pen,xPos+radius,yPos+radius,radius*2,radius*2);
        }
        /// <summary>
        /// Method to draw a filled circle
        /// </summary>
        /// <param name="radius">radius of the circle</param>
        public void DrawFillCircle(int radius)
        {
            g.FillEllipse(Brush, xPos, yPos, radius, radius);
        }
        /// <summary>
        /// Method to draw triangle
        /// </summary>
        /// <param name="points">base,adjacent and hypotenuse</param>
        public void DrawTriangle( PointF [] points)
        {
            g.DrawPolygon(Pen,points);
        }
        /// <summary>
        /// Method to draw filled triangle
        /// </summary>
        /// <param name="points">base,adjacent and hypotenuse</param>
        public void DrawFillTriangle(PointF[] points)
        {
            g.FillPolygon(Brush, points);
        }
        /// <summary>
        /// Method to draw rectangle
        /// </summary>
        /// <param name="width">width of the rectangle</param>
        /// <param name="height">height of the rectangle</param>
        public void DrawRectangle (int width,int height)
        {
            g.DrawRectangle(Pen, xPos, yPos,  width,  height);
        }
        /// <summary>
        /// Method to draw filled rectangle
        /// </summary>
        /// <param name="width">width of the rectangle</param>
        /// <param name="height">height of the rectangle</param>
        public void DrawFillRectangle(int width, int height)
        {
            g.FillRectangle(Brush, xPos, yPos,  width, height);
        }
        /// <summary>
        /// This method to clear the output window
        /// </summary>
        public void fnClear()
        {
            g.Clear(Color.White);
        }
        /// <summary>
        /// Method will reset cursor position to original position 
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y Position</param>
        public void fnReset(int x,int y)
        {
            xPos = x;
            yPos = y;
        }
        /// <summary>
        /// Method to change the pen color to black
        /// </summary>
        public void updateBlackPen()
        {
            // Pen = new Pen(Color.Col, 1);
            Pen.Color = System.Drawing.Color.Black;
        }
        /// <summary>
        /// Method to change the pen color to green
        /// </summary>
        public void updateGreenPen()
        {
            // Pen = new Pen(Color.Col, 1);
            Pen.Color = System.Drawing.Color.Green;
        }
        /// <summary>
        /// Method to change the pen color to blue
        /// </summary>
        public void updateBluePen()
        {
            // Pen = new Pen(Color.Col, 1);
            Pen.Color = System.Drawing.Color.Blue;
        }
        /// <summary>
        /// Method to validate the commamd and display the error message in output window 
        /// </summary>
        /// <param name="y">Line that is invalid in the code</param>
        public void fnValidCommand(float y)
        {
            // Create string to draw.
            String drawString = "Invalid Command" + System.Environment.NewLine; ;

            // Create font and brush.
            Font drawFont = new Font("Arial", 12);
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            // Create point for upper-left corner of drawing.
            float x = 200.0F;
            //float y = 10.0F;

            // Set format of string.
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            g.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);
            return;
        }
        /// <summary>
        /// Method to validate the parameter and display the error message in output window 
        /// </summary>
        /// <param name="y">Line that is invalid in the code</param>

        public void fnNonNumeric(float y)
        {
            // Create string to draw.
            String drawString = "Non numeric parameter" + System.Environment.NewLine; ;

            // Create font and brush.
            Font drawFont = new Font("Arial", 12);
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            // Create point for upper-left corner of drawing.
            float x = 200.0F;
            //float y = 10.0F;

            // Set format of string.
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            g.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);
            return;
        }
        /// <summary>
        /// Method to validate the commamd and display the error message in output window 
        /// </summary>
        /// <param name="y">Line that is invalid in the code</param>
        public void fnValidCommand(int oLineno , float y )
        {
            // Create string to draw.
            String drawString = "Invalid Command at line " + oLineno + System.Environment.NewLine;

            // Create font and brush.
            Font drawFont = new Font("Arial", 10);
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            // Create point for upper-left corner of drawing.
            float x = 200.0F;
            //float y = 20.0F;

            // Set format of string.
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            g.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);
            Console.WriteLine("coming c");
            return;
        }
        /// <summary>
        /// Method to validate the parameter and display the error message in output window 
        /// </summary>
        /// <param name="y">Line that is invalid in the code</param>
        public void fnNonNumeric(int oLineno , float y)
        {
            // Create string to draw.
            String drawString = "Non numeric parameter at line "
            + oLineno + System.Environment.NewLine ;

            // Create font and brush.
            Font drawFont = new Font("Arial", 10);
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            // Create point for upper-left corner of drawing.
            float x = 200.0F;
            //float y = 10.0F;

            // Set format of string.
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            g.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);
            Console.WriteLine("coming c");
            return;
        }
    }

}
