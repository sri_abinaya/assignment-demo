﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace Assignment1
{
    /// <summary>
    /// This class is the entry point of this program
    /// Written by Sri Abinaya
    /// </summary>
    public partial class Form1 : Form
    {

        Bitmap OutputBitmap = new Bitmap(640, 480);
        Canvas MyCanvas;
        Boolean ofill = false;
        int flash = 1;
        Thread newthread;
        /// <summary>
        /// In this form initiated canvas and thread
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            MyCanvas = new Canvas(Graphics.FromImage(OutputBitmap));
            newthread = new Thread(threadMethod);
            newthread.Start();
        }
        /// <summary>
        /// Threadmethod continuously run and keep on flashing three mixed colors alternatively
        /// </summary>
        public void threadMethod()
        {
            while (true)
            {
                
                if(flash == 1)
                {
                    this.OutputWindow.BackColor = Color.FromArgb(255, 255, 0);
                    flash++;
                    
                }
                else if (flash==2)
                {
                    this.OutputWindow.BackColor = Color.FromArgb(0, 255, 0);
                    flash++;
                   
                }
                else if(flash == 3)
                {
                    this.OutputWindow.BackColor = Color.FromArgb(128, 128, 128);
                    flash = 1;
                }
                Thread.Sleep(500);
            }
        }
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Once the text entered and hit the enter button it will start to execute
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> event which keydown </param>
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Enter)
            {
                float y = 0.0F;
                String Command = commandLine.Text.Trim().ToLower();
                String []aCommand =Command.Split(' ');
                int xPos=0, yPos=0;
                bool bXPos = true, bYPos = true;
                String Col="";
                //Console.WriteLine(aCommand[0]);
                if (aCommand[0].Equals("circle")&&(aCommand.Length==2))
                {
                    bXPos = int.TryParse(aCommand[1], out xPos);
                    if (bXPos == true)
                    {
                        xPos = Int32.Parse(aCommand[1]);
                    }
                    else
                    {
                        y += 13.0F;
                        MyCanvas.fnNonNumeric(y);
                    }
                    
                }
                else if (aCommand[0].Equals("clear") || aCommand[0].Equals("triangle") || aCommand[0].Equals("pen") ||
                    aCommand[0].Equals("fill") || aCommand[0].Equals("reset"))
                {
                    xPos = 0;
                    yPos = 0;
                    if (aCommand[0].Equals("pen"))
                    {
                        Col = aCommand[1].Trim().ToLower();
                        Console.WriteLine(aCommand[1]);
                    }
                    else if (aCommand[0].Equals("fill"))
                    {
                        if (aCommand[1].Trim().ToLower().Equals("on"))
                        {
                            ofill = true;
                        }
                        else
                        {
                            ofill = false;
                        }
                    }
                }
                else if (aCommand[0].Equals("drawto") || aCommand[0].Equals("moveto") || aCommand[0].Equals("rect"))
                {
                    String[] aParameters = aCommand[1].Split(',');
                    //int xPos = 0, yPos = 0;
                    if (aParameters.Length == 2)
                    {
                        bXPos = int.TryParse(aParameters[0], out xPos);
                        bYPos = int.TryParse(aParameters[1], out yPos);
                        if ((bXPos == false) || (bYPos == false))
                        {
                            y += 13.0F;
                            MyCanvas.fnNonNumeric(y);

                        }
                    }else if(aParameters.Length != 2)
                    {
                        
                        MyCanvas.fnValidCommand(y);
                    }
                }
                
                else
                {
                    y += 13.0F;
                    //throw new Exception("InvalidCommand");
                    MyCanvas.fnValidCommand(y);


                }
                if ((aCommand[0].Equals("drawto") == true)&&(bXPos==true)&&(bYPos==true))
                {
                    MyCanvas.DrawLine(xPos, yPos);
                    Console.WriteLine("Line");
                }
                else if (aCommand[0].Equals("rect") == true)
                {
                    if (ofill == false)
                        MyCanvas.DrawRectangle(xPos, yPos);
                    else
                        MyCanvas.DrawFillRectangle(xPos, yPos);
                    Console.WriteLine("Rectangle");
                }
                else if (aCommand[0].Equals("triangle") == true)
                {
                    PointF point1 = new PointF(10.0F, 10.0F);
                    PointF point2 = new PointF(200.0F, 50.0F);
                    PointF point3 = new PointF(80.0F, 150.0F);
                    PointF[] curvePoints =
                  {
                 point1,
                 point2,
                 point3
                    };
                    if (ofill == false)
                    {
                        MyCanvas.DrawTriangle(curvePoints);
                    }
                    else if (ofill == true)
                    {
                        MyCanvas.DrawFillTriangle(curvePoints);
                    }
                    Console.WriteLine("Rectangle");
                }
                else if (aCommand[0].Equals("moveto") == true)
                {
                    MyCanvas.MoveTo(xPos, yPos);
                    Console.WriteLine("moveto");
                }
                else if (aCommand[0].Equals("pen") == true)
                {
                    if (Col.Equals("black"))
                    {
                        MyCanvas.updateBlackPen();
                    }
                    else if (Col.Equals("green"))
                    {
                        MyCanvas.updateGreenPen();
                    }
                    else if (Col.Equals("blue"))
                    {
                        MyCanvas.updateBluePen();
                    }

                    Console.WriteLine("moveto");
                }
                else if (aCommand[0].Equals("circle") == true)
                {
                    if (ofill == false)
                        MyCanvas.DrawCircle(xPos);
                    else
                        MyCanvas.DrawFillCircle(xPos);
                    Console.WriteLine("circle");
                }
                else if (aCommand[0].Equals("clear") == true)
                {
                    MyCanvas.fnClear();
                    
                }
                else if (aCommand[0].Equals("reset") == true)
                {
                    MyCanvas.fnReset(xPos,yPos);
                }
                    commandLine.Text = "";
                Refresh();
            }
        }
        /// <summary>
        /// Once the text entered in program windown and hit the run button the following method get called and execute
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">event which key down</param>
        private void button1_Click(object sender, EventArgs e)
        {
            String line = ProgramWindow.Text.Trim().ToLower();
            String[] lines = line.Split('\n');
            int n = lines.Length;
            String[] vname = new string[0];
            var varList = new List<KeyValuePair<string, int>>();
            Dictionary <string, int> varDict = new Dictionary<string, int>();
            int[] varvalues = new int[0];
            Boolean ofill = false;
            Boolean oSetVar = false;
            Boolean oLoopflag = false;
            var oIteration = 0;
            var oLoopsize = 0;
            var oLoopCounter = 0;
            var owProgramCounter = 0;
            int wPrefixval = 0;
            int wSufixval = 0;
            int oLine = 0;
            int oValue = 0;
            Boolean ifExecuteLineflag = true;
            float y = 0.0F;

            ///Initialize the variable counter to 0
            int oVC = 0;
            for (int i = 0;i < n; i++)
            {
                
                String str = lines[i].Trim();
                    oVC++;
                String[] aCommand = str.Split(' ');
                int xPos = 0, yPos = 0;
                String Col = "";
                oLine = oLine + 1;
                bool bXPos = true, bYPos = true;
                /*if ((bXPos == false) || (bYPos == false))
                {
                    MyCanvas.fnNonNumeric();

                }*/
                if (str.Contains("=") && oLoopflag == false)
                {   ///Split the variable 
                   
                     String oVariable = str.Split('=')[0].Trim();
                     String sValue = str.Split('=')[1];
                     Boolean bValue = int.TryParse(sValue, out oValue);
                    if (sValue.Contains("+"))
                    {
                        String[] ofull = sValue.Split('+');
                        var isPrefix = int.TryParse(ofull[0].Trim(), out int oPrefix);
                        var isSuffix = int.TryParse(ofull[1].Trim(), out int oSuffix);
                        Console.WriteLine(isPrefix);
                        Console.WriteLine(isSuffix);
                        Console.WriteLine(oPrefix);
                        Console.WriteLine(oSuffix);
                        if (isPrefix == false && isSuffix == true)
                        {
                            Console.WriteLine("abn");
                            Console.WriteLine(ofull[0]);
                            Console.WriteLine(ofull[1]);
                            Console.WriteLine("abn");
                            if (varDict.ContainsKey(ofull[0].Trim()))
                            {
                                Console.WriteLine("aa");
                                int preffixValue = varDict[ofull[0].Trim()];
                                oValue = preffixValue + oSuffix;
                                if (!varDict.ContainsKey(oVariable))
                                {
                                    //vname[oVC] = oVariable[0];
                                    varDict.Add(oVariable, oValue);
                                    Console.WriteLine(oVariable);
                                    Console.WriteLine(oValue);

                                }
                                else
                                {
                                    varDict[oVariable] = oValue;
                                }
                            }
                        }else if (isSuffix == false && isPrefix == true)
                        {
                           
                            if (varDict.ContainsKey(ofull[1]))
                            {
                                int suffixValue = varDict[ofull[1].Trim()];
                                oValue = oPrefix + suffixValue;
                                if (!varDict.ContainsKey(oVariable))
                                {
                                    //vname[oVC] = oVariable[0];
                                    varDict.Add(oVariable, oValue);
                                    Console.WriteLine(oVariable);
                                    Console.WriteLine(oValue);

                                }
                            }
                        }
                        else if((isSuffix== false)&&(isPrefix==false))
                        {
                            int preffixValue = 0;
                            int suffixValue = 0;
                            if (varDict.ContainsKey(ofull[0].Trim()))
                            {
                                preffixValue = varDict[ofull[0].Trim()];
                                
                                
                            }
                            if (varDict.ContainsKey(ofull[1].Trim()))
                            {
                                suffixValue = varDict[ofull[1].Trim()];
                            }

                            oValue = preffixValue + suffixValue;
                            Console.WriteLine(oValue);
                            if (!varDict.ContainsKey(oVariable))
                            {
                                //vname[oVC] = oVariable[0];
                                varDict.Add(oVariable, oValue);
                            }
                            }

                        }
                    else if (sValue.Contains("-"))
                    {

                    }
                    else if (sValue.Contains("*"))
                    {

                    }
                    else if (sValue.Contains("/"))
                    {

                    }
                    else
                    {
                        oValue = Int32.Parse(str.Split('=')[1]);
                        Console.WriteLine(sValue);
                    }
                    ///check the specific varible found in the vname array using contains method
                    if (!varDict.ContainsKey(oVariable))
                    {
                        //vname[oVC] = oVariable[0];
                        varDict.Add(oVariable, oValue);
                        Console.WriteLine("yay");
                        Console.WriteLine(oVariable);
                        Console.WriteLine(oValue);

                    }
                    else
                    {
                        Console.WriteLine(oVariable + " is already there");
                    }
                    oSetVar = true;
                }
               else  if (aCommand[0].Equals("circle") && oLoopflag == false)
                {
                    if (aCommand.Length == 2)
                    {
                        Console.WriteLine("There = circle");
                        bXPos = int.TryParse(aCommand[1], out xPos);
                        if (bXPos == false)
                        {
                            if (varDict.ContainsKey(aCommand[1])){

                            }
                        }
                        Console.WriteLine(bXPos);
                        if (bXPos == true)
                            xPos = Int32.Parse(aCommand[1]);
                        else if (bXPos == false && oSetVar == false)
                        {
                            
                            y += 13.0F;
                            MyCanvas.fnNonNumeric(oLine, y);
                        }
                    }
                    else if (aCommand.Length != 2)
                    {
                        y += 13.0F;
                        MyCanvas.fnValidCommand(oLine, y);
                        
                    }
                }
                else if (aCommand[0].Equals("clear")  || aCommand[0].Equals("pen") ||
                    aCommand[0].Equals("fill") || aCommand[0].Equals("reset"))
                {
                    xPos = 0;
                    yPos = 0;
                    if (aCommand[0].Equals("pen"))
                    {
                        Col = aCommand[1].Trim().ToLower();
                       
                    }
                    else if (aCommand[0].Equals("fill"))
                    {
                        if (aCommand[1].Trim().ToLower().Equals("on"))
                        {
                            ofill = true;
                        }
                        else
                        {
                            ofill = false;
                        }
                    }
                }
                else if (aCommand[0].Equals("drawto") || aCommand[0].Equals("moveto") || aCommand[0].Equals("rect") || aCommand[0].Equals("tri"))
                {


                    Console.WriteLine(aCommand[0]);
                    Console.WriteLine(aCommand[1]);
                    String[] aParameters = aCommand[1].Split(',');
                    Console.WriteLine("tri");
                    Console.WriteLine(aParameters.Length);
                    if (aParameters.Length == 2)
                    {
                        bXPos = int.TryParse(aParameters[0], out xPos);
                        bYPos = int.TryParse(aParameters[1], out yPos);
                        //xPos = Int32.Parse(aParameters[0]);
                        //yPos = Int32.Parse(aParameters[1]);
                        Console.WriteLine(xPos);
                        Console.WriteLine(yPos);
                        if ((bXPos == false) || (bYPos == false))
                        {
                            y += 13.0F;
                            MyCanvas.fnNonNumeric(oLine, y);

                        }
                        else if ((bXPos == true) && (bYPos == true))
                        {
                            xPos = Int32.Parse(aParameters[0]);
                            yPos = Int32.Parse(aParameters[1]);
                            Console.WriteLine(xPos);
                            Console.WriteLine(yPos);
                        }
                    }else if (aParameters.Length != 2)
                    {
                        y += 13.0F;
                        MyCanvas.fnValidCommand(oLine, y);
                        Console.WriteLine("end tri");
                       
                    }
                }
                if (aCommand[0].Equals("drawto") == true)
                {
                    MyCanvas.DrawLine(xPos, yPos);
                    Console.WriteLine("Line");
                }
                else if (aCommand[0].Equals("rect") == true && ifExecuteLineflag)
                {
                    if (ofill == false)
                        MyCanvas.DrawRectangle(xPos, yPos);
                    else
                        MyCanvas.DrawFillRectangle(xPos, yPos);
                    Console.WriteLine("Rectangle");
                }
                else if (aCommand[0].Equals("tri") == true && ifExecuteLineflag)
                {
                    PointF point1 = new PointF(yPos, yPos);
                    PointF point2 = new PointF(xPos *2 , xPos + yPos );
                    PointF point3 = new PointF(xPos,yPos * 2);

                    PointF[] curvePoints =
                  {
                 point1,
                 point2,
                 point3
              
                    };
                    if (ofill == false)
                    {
                        MyCanvas.DrawTriangle(curvePoints);
                    }
                    else if (ofill == true)
                    {
                        MyCanvas.DrawFillTriangle(curvePoints);
                    }
                    Console.WriteLine("Rectangle");
                }
                else if (aCommand[0].Equals("moveto") == true)
                {
                    MyCanvas.MoveTo(xPos, yPos);
                    Console.WriteLine("moveto");
                }
                else if (aCommand[0].Equals("pen") == true)
                {
                    if (Col.Equals("black"))
                    {
                        MyCanvas.updateBlackPen();
                    }
                    else if (Col.Equals("green"))
                    {
                        MyCanvas.updateGreenPen();
                    }
                    else if (Col.Equals("blue"))
                    {
                        MyCanvas.updateBluePen();
                    }

                    Console.WriteLine("moveto");
                }
                else if (aCommand[0].Equals("circle") == true && oLoopflag == false && ifExecuteLineflag)
                {
                    bXPos = int.TryParse(aCommand[1], out xPos);
                    if (bXPos == false)
                    {
                        Console.WriteLine("hey");
                        int svalue;
                        bool hasValue = varDict.TryGetValue(aCommand[1], out svalue);
                        if (varDict.ContainsKey(aCommand[1].Trim()))
                        { 
                            xPos = varDict[aCommand[1]];
                        }
                    }
                    if (ofill == false)
                        MyCanvas.DrawCircle(xPos);
                    else
                        MyCanvas.DrawFillCircle(xPos);
                    Console.WriteLine("circle");
                }
                else if (aCommand[0].Equals("clear") == true)
                {
                    MyCanvas.fnClear();

                }
                else if (aCommand[0].Equals("reset") == true)
                {
                    MyCanvas.fnReset(xPos, yPos);
                }
                else if (aCommand[0].Equals("while"))
                {
                    Console.WriteLine("while");
                    oLoopflag = true;
                    var iscPrefix = int.TryParse(aCommand[1].Trim(), out wPrefixval);
                    var iscSufix = int.TryParse(aCommand[3].Trim(), out wSufixval);
                    var cPrefixValue = varDict[aCommand[1].Trim()];
                    Console.WriteLine(cPrefixValue);
                    oIteration = wSufixval - cPrefixValue;
                    Console.WriteLine(oIteration);
                    oLoopsize = 0;
                    oLoopCounter = 0;

                }

                else if (aCommand[0].Equals("endwhile"))
                {
                    Console.WriteLine("EndLoop");
                    oLoopflag = false;

                    owProgramCounter = owProgramCounter - oLoopsize;
                    if (oLoopCounter++ < oIteration)
                    {
                        int indexNew = oLoopsize;
                        Console.WriteLine(indexNew);
                        i = oLoopsize - 1;
                    }
                }
                else if (oLoopflag == true)
                {
                    Console.WriteLine("vivi");
                    oLoopsize = oLoopsize + 1;
                }
                else if (aCommand[0].Equals("if"))
                {
                    var oIfvariable = varDict[aCommand[1].Trim()];
                    var ifSufix = int.TryParse(aCommand[3].Trim(), out int ifSufixval);

                    if (aCommand[2].Contains('<'))
                    {
                        if (oIfvariable < ifSufixval)
                        {
                            ifExecuteLineflag = true;
                        }
                        else
                        {
                            ifExecuteLineflag = false;
                        }

                    }
                    else if (aCommand[2].Contains('>'))
                    {
                        if (oIfvariable > ifSufixval)
                        {
                            ifExecuteLineflag = true;
                        }
                        else
                        {
                            ifExecuteLineflag = false;
                        }
                    }
                    else
                    {
                        if (oIfvariable == ifSufixval)
                        {
                            ifExecuteLineflag = true;
                        }
                        else
                        {
                            ifExecuteLineflag = false;
                        }
                    }
                }
                else if (aCommand[0].Equals("endif"))
                {
                    ifExecuteLineflag = true;
                }
                else if (aCommand[0].Equals("color"))
                {
                    int red = 0; int green = 0;
                    Random rnd = new Random();
                    if (aCommand[1].Equals("redgreen")){
                        int blink = 3;
                        for (int j = 0; j < 3; j++)
                        {
                            this.OutputWindow.BackColor = Color.FromArgb(255, 255, 0);
                            Thread.Sleep(10000);
                        }
                        this.OutputWindow.BackColor = Color.FromArgb(255, 255, 255);
                    }
                    else if (aCommand[1].Equals("blueyellow"))
                    {
                        this.OutputWindow.BackColor = Color.FromArgb(0, 255, 0);
                        Thread.Sleep(1000);
                        this.OutputWindow.BackColor = Color.FromArgb(255, 255, 255);
                    }
                    else if (aCommand[1].Equals("blackwhite"))
                    {
                        this.OutputWindow.BackColor = Color.FromArgb(128, 128, 128);
                        Thread.Sleep(1000);
                        this.OutputWindow.BackColor = Color.FromArgb(255, 255, 255);
                    }
                }
                else if(oSetVar== false)
                {
                    Console.WriteLine("else");
                    y += 13.0F;
                    MyCanvas.fnValidCommand(oLine,y);
                }

            }

            ProgramWindow.Text = "";
            Refresh();
        }
        /// <summary>
        /// To check the syntax of the code which typed in program window the below method get called
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">event which key down</param>

        private void button2_Click(object sender, EventArgs e)
        {
            String line = ProgramWindow.Text.Trim().ToLower();
            String[] lines = line.Split('\n');
            Boolean ofill = false;
            int oLine = 0;
            float y = 0.0F;
            Dictionary<string, int> varDict = new Dictionary<string, int>();
            int oValue = 0;
            foreach (String str in lines)
            {
                Console.WriteLine(str);
                String[] aCommand = str.Split(' ');
                int xPos = 0, yPos = 0;
                String Col = "";
                oLine = oLine + 1;
                bool bXPos = true, bYPos = true;
                if (str.Contains("="))
                {
                    String oVariable = str.Split('=')[0].Trim();
                    String sValue = str.Split('=')[1].Trim();
                    Boolean bValue = int.TryParse(sValue, out oValue);
                    if (!varDict.ContainsKey(oVariable))
                    {
                        varDict.Add(oVariable, oValue);
                    }
                }
                else if (aCommand[0].Equals("circle"))
                {
                    //Console.WriteLine(aCommand[1]);
                    if (aCommand.Length != 2)
                    {
                        y += 13.0F;
                        MyCanvas.fnValidCommand(oLine, y);
                    }
                    else
                    {
                        bXPos = int.TryParse(aCommand[1], out xPos);
                        if (varDict.ContainsKey(aCommand[1].Trim()))
                        {
                            bXPos = true;
                        }
                        if (bXPos == false && varDict.ContainsKey(aCommand[1])==false)
                        {
                            y += 13.0F;
                            MyCanvas.fnNonNumeric(oLine, y);
                        }


                    }
                }
                else if (aCommand[0].Equals("endloop") || aCommand[0].Equals("endif")|| aCommand[0].Equals("while")||aCommand[0].Equals("if"))
                {
                    continue;
                }
                else if (aCommand[0].Equals("drawto") || aCommand[0].Equals("moveto") || aCommand[0].Equals("rect") || aCommand[0].Equals("tri"))
                {

                    String[] aParameters = aCommand[1].Split(',');
                    if (aParameters.Length == 2)
                    {
                        bXPos = int.TryParse(aParameters[0], out xPos);
                        bYPos = int.TryParse(aParameters[1], out yPos);

                        //xPos = Int32.Parse(aParameters[0]);
                        //yPos = Int32.Parse(aParameters[1]);

                        if ((bXPos == false) || (bYPos == false))

                        {
                            y += 13.0F;
                            MyCanvas.fnNonNumeric(oLine, y);
                            Console.WriteLine(oLine);
                            Console.WriteLine(y);

                        }
                    }
                    else if (aParameters.Length != 3)
                    {
                        MyCanvas.fnValidCommand(oLine, y);
                    }
                }
                else
                {
                    y += 13.0F;
                    MyCanvas.fnValidCommand(oLine, y);
                }
            }
            //ProgramWindow.Text = "";
            Refresh();
        }
        /// <summary>
        /// To initialize the graphics in the output windown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> event which key down</param>
        private void OutputWindow_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(OutputBitmap, 0, 0);

        }
    }
}
